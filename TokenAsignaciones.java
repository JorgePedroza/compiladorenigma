import java.io.PrintStream;
import java.util.Hashtable;

import javax.swing.plaf.synth.SynthOptionPaneUI;

import java.lang.String;
import java.util.ArrayList;
import java.util.Enumeration;

//ENIGMA
class TokenAsignaciones {
	// Variable para validar asignaciones a caracteres(ichr)
	public static int segunda = 0;
	static Token tokenOperador;
	
	// Tabla que almacenara los tokens declarados
	private static Hashtable tabla = new Hashtable();

	// Listas que guardaran los tipos compatibles de las variables
	private static ArrayList<Integer> intComp = new ArrayList();
	private static ArrayList<Integer> decComp = new ArrayList();
	private static ArrayList<Integer> strComp = new ArrayList();
	private static ArrayList<Integer> chrComp = new ArrayList();
	private static ArrayList<Integer> boolComp = new ArrayList();
	private static ArrayList<Integer> OperComp = new ArrayList();
	static String identificadores[] = new String[100];
	static int band=0;
	static int cont=0;
	// variable //tipoDato
	public static void InsertarSimbolo(Token identificador, int tipo) {
		// System.out.println("SOYYYY EL IDENTIFICADOR" +identificador.image+" SOY
		// ELTIPO DE DATO"+ tipo);
		// En este metodo se agrega a la tabla de tokens el identificador que esta
		// siendo declarado junto con su tipo de dato		
		
		x = 1;
		int j;
		for(j=0; 0 < identificadores.length; j++){
			if(identificadores[j] == null)
				break;
			else{
				//System.out.println(identificadores[j]+" - "+identificador.image);
				if(identificadores[j].equals(identificador.image)){
					System.out.println("Error sem\u00e1ntico: El identificador <" + identificador.image + "> esta duplicado --> Linea: "
					+ identificador.beginLine + ", Columna: " + identificador.beginColumn);
				}
			}
		}
		identificadores[j] = identificador.image;
		tabla.put(identificador.image, tipo);
	}

	public static void SetTables() {
		/*
		 * En este metodo se inicializan las tablas, las cuales almacenaran los tipo de
		 * datos compatibles con: /* 
		 * entero = int 8(44) - 44(48) 
		 * decimal = double 9(45)- 45(50) 
		 * cadena = string 10(47) - 48(51) 
		 * boolean = boolean 11 - 46 y 47
		 * caracter = char 12(46) - 49(52) 
		 * kind 50 es el de identificadores
		 * 
		 */
		intComp.add(8);
		intComp.add(44);

		decComp.add(9);
		decComp.add(44);
		decComp.add(45);

		chrComp.add(12);
		chrComp.add(49);

		strComp.add(10);
		strComp.add(48);

		boolComp.add(11);
		boolComp.add(46);
		boolComp.add(47);

		//Oper.add();

	}

	// tokenIzq = nombre del identificador y TokenAsig es el dato o valor de la
	// variable
	public static String checkAsing(Token TokenIzq, Token TokenAsig, String TokenOper, boolean oper) {
		
				//	System.out.println("-<<"+TokenOper);
					//System.out.println("->>"+tipoIdent2);
	
	
		if (tabla.contains(TokenIzq)) {
			System.out.println("Error de unicidad: El identificador esta duplicado");
			return "";
		} else {

			// variables en las cuales se almacenara el tipo de dato del identificador y de
			// las asignaciones (ejemplo: n1(tipoIdent1) = 2(tipoIdent2) + 3(tipoIdent2))
			int tipoIdent1;
			int tipoIdent2;

			/*
			 * De la tabla obtenemos el tipo de dato del identificador asi como, si el token
			 * enviado es diferente a algun tipo que no se declara como los numeros(48), los
			 * decimales(50), caracteres(52) y cadenas(51) entonces tipoIdent1 =
			 * tipo_de_dato, ya que TokenAsig es un dato
			 */
			if (TokenIzq.kind != 44 && TokenIzq.kind != 49) {

				try {
					// Si el TokenIzq.image existe dentro de la tabla de tokens, entonces tipoIdent1
					// toma el tipo de dato con el que TokenIzq.image fue declarado
					tipoIdent1 = (Integer) tabla.get(TokenIzq.image);
				} catch (Exception e) {
					// Si TokenIzq.image no se encuentra en la tabla en la cual se agregan los
					// tokens, el token no ha sido declarado, y se manda un error
					return "Error sem\u00e1ntico: El identificador <" + TokenIzq.image + "> no ha sido declarado --> Linea: "
					+ TokenIzq.beginLine + ", Columna: " + TokenIzq.beginColumn;
				}
			} else
				tipoIdent1 = 0;

			// TokenAsig.kind != 48 && TokenAsig.kind != 50 && TokenAsig.kind != 51 &&
			// TokenAsig.kind != 52
			if (TokenAsig.kind == 50)
			{
				/*
				 * Si el tipo de dato que se esta asignando, es algun identificador(kind == 49)
				 * se obtiene su tipo de la tabla de tokens para poder hacer las comparaciones
				 */
				try {
					tipoIdent2 = (Integer) tabla.get(TokenAsig.image);
					//System.out.println(tipoIdent1 + " ---- " + tipoIdent2);
				} catch (Exception e) {
					// si el identificador no existe manda el error
					return "Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no ha sido declarado --> Linea: "
					+ TokenAsig.beginLine+ ", Columna: " + TokenAsig.beginColumn;
				}
			}
			// Si el dato es entero(44) o decimal(45) o String(48) o char(49) o falso(46) o
			// verdadero(47)
			// tipoIdent2 = tipo_del_dato
			else if (TokenAsig.kind == 44 || TokenAsig.kind == 45 || TokenAsig.kind == 48 || TokenAsig.kind == 49
					|| TokenAsig.kind == 46 || TokenAsig.kind == 47)
				tipoIdent2 = TokenAsig.kind;

			else // Si no, se inicializa en algun valor "sin significado(con respecto a los
					// tokens)", para que la variable este inicializada y no marque error al
					// comparar
				tipoIdent2 = 0;
			/*
			 * entero = int 8(44) - 44(48) decimal = double 9(45) - 45(50) cadena = string
			 * 10(47) - 48(51) boolean = boolean 11 - 46 y 47 caracter = char 12(46) -
			 * 49(52)
			 */

			if (tipoIdent1 == 8) // Int
			{
				// System.out.println("--entro");
				// Si la lista de enteros(intComp) contiene el valor de tipoIdent2, entonces es
				// compatible y se puede hacer la asignacion
				if (intComp.contains(tipoIdent2))
					return " ";
				else // Si el tipo de dato no es compatible manda el error
					return "Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no se puede convertir a tipo int --> Linea: "
								+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
			} else if (tipoIdent1 == 9) // double
			{
				if (decComp.contains(tipoIdent2))
					return " ";
				else
					return "Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no se puede convertir a tipo double --> Linea: "
								+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
							
			} else if (tipoIdent1 == 12) // char
			{
				/*
				 * variable segunda: cuenta cuantos datos se van a asignar al caracter: si a el
				 * caracter se le asigna mas de un dato (ej: 'a' + 'b') marca error NOTA: no se
				 * utiliza un booleano ya que entraria en asignaciones pares o impares
				 */
				
				if(oper){
							System.out.println("Error sem\u00e1ntico: No es posible realizar operaciones aritm\u00E9ticas con este tipo de dato --> Linea: "
							+ TokenAsig.beginLine+ ", Columna: "+ TokenAsig.beginColumn);
				  return " ";
				  }
				

				segunda++;
				if (segunda < 2) {
					if (chrComp.contains(tipoIdent2))
						return " ";
					else
						return 	"Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no se puede convertir a tipo char --> Linea: "
								+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
				} return "";

			} 
			
			
			
			else if (tipoIdent1 == 10) // string
			{
				//System.out.println("---"+TokenOper);

				if(TokenOper != "+"){
						return 
						"Error sem\u00e1ntico: No es posible realizar esta operaci\u00F3n aritm\u00E9tica con Strings --> Linea: "
								+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
				}


				if (strComp.contains(tipoIdent2)){
				//	System.out.println("-<<"+tipoIdent1);
				//	System.out.println("->>"+tipoIdent2);
					return " ";
				}
				else
					return "Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no se puede convertir a tipo String --> Linea: "
								+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
			} else if (tipoIdent1 == 11) // boolean
			{	
				if(oper){
					return "Error sem\u00e1ntico: No es posible realizar esta operaci\u00F3n aritm\u00E9tica con booleanos --> Linea: "
								+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
				}

				if (boolComp.contains(tipoIdent2))
					return " ";
				else // Si el tipo de dato no es compatible manda el error
					return "Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no se puede convertir a tipo boolean --> Linea: "
					+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
			} else {
				return "Error sem\u00e1ntico: El identificador <" + TokenAsig.image + "> no ha sido declarado --> Linea: "
				+ TokenAsig.beginLine + ", Columna: " + TokenAsig.beginColumn;
			}
		}
	}

	/*
	 * Metodo que verifica si un identificador ha sido declarado, ej cuando se
	 * declaran las asignaciones: i++, i--)
	 */
public static String checkVariable(Token checkTok, boolean banderaFor) {
				try {
			// Intenta obtener el token a verificar(checkTok) de la tabla de los tokens
			int tipoIdent1 = (Integer) tabla.get(checkTok.image);
				if(tipoIdent1 != 8 && banderaFor==true){
					return "Error sem\u00e1ntico: El identificador <" + checkTok.image + "> no es de un tipo compatible con la estructura <int> --> Linea: "
				+ checkTok.beginLine + ", Columna: " + checkTok.beginColumn;
				}
				return " ";
			
		} catch (Exception e) {
			if (checkTok.kind != 44 && checkTok.kind != 49 && checkTok.kind != 45 && checkTok.kind != 48 && checkTok.kind != 46 && checkTok.kind != 47) {
			// Si no lo puede obtener, manda el error
					return "Error sem\u00e1ntico: El identificador <" + checkTok.image + "> no ha sido declarado --> Linea: "
				+ checkTok.beginLine + ", Columna: " + checkTok.beginColumn;
			}
			return " ";
		}
	}



public static String checkComparable(Token tokenIzq,Token tokenOper, Token tokenDer ){
	int tipoIdent1;
	int tipoIdent2 = 0;

	
	
	//hay que comparar los tipos de datos, no los datos como tal
   //---------valores---int------------------char-------------------double-----------------String---------------true---------------------false-----
	if (tokenIzq.kind != 44 && tokenIzq.kind != 49 && tokenIzq.kind != 45 && tokenIzq.kind != 48 && tokenIzq.kind != 46 && tokenIzq.kind != 47) {

				try {
					// Si el TokenIzq.image existe dentro de la tabla de tokens, entonces tipoIdent1
					// toma el tipo de dato con el que TokenIzq.image fue declarado
					tipoIdent1 = (Integer) tabla.get(tokenIzq.image);
					
				} catch (Exception e) {
					// Si TokenIzq.image no se encuentra en la tabla en la cual se agregan los
					// tokens, el token no ha sido declarado, y se manda un error
					return "Error sem\u00e1ntico: El identificador <" + tokenIzq.image + "> no ha sido declarado --> Linea: "
							+ tokenIzq.beginLine + ", Columna: " + tokenIzq.beginColumn;
				}
				
			} else{
				tipoIdent1 = tokenIzq.kind;
				
				switch(tipoIdent1){
					case 44:
					tipoIdent1 = 8; 	break;
					case 45:
					tipoIdent1 = 9;		break;
					case 48:
					tipoIdent1 = 10;	break;
					case 46: 
					tipoIdent1 = 11;	break;
					case 47:
					tipoIdent1 = 11;	break;
					case 49:
					tipoIdent1 = 12;    break;
					default:
					break;
				}
			}
				

	if (tokenDer.kind != 44 && tokenDer.kind != 49 && tokenDer.kind != 45 && tokenDer.kind != 48) {

				try {
					// Si el TokenIzq.image existe dentro de la tabla de tokens, entonces tipoIdent1
					// toma el tipo de dato con el que TokenIzq.image fue declarado
					tipoIdent2 = (Integer) tabla.get(tokenDer.image);
					
				} catch (Exception e) {
					// Si TokenIzq.image no se encuentra en la tabla en la cual se agregan los
					// tokens, el token no ha sido declarado, y se manda un error
					return "Error sem\u00e1ntico: El identificador <" + tokenDer.image + "> no ha sido declarado --> Linea: "
							+ tokenDer.beginLine + ", Columna: " + tokenDer.beginColumn;
				}
			} else{
				tipoIdent2 = tokenDer.kind;
				
				switch(tipoIdent2){
					case 44:
					tipoIdent2 = 8; 	break;
					case 45:
					tipoIdent2 = 9;		break;
					case 48:
					tipoIdent2 = 10;	break;
					case 46: 
					tipoIdent2 = 11;	break;
					case 47:
					tipoIdent2 = 11;	break;
					case 49:
					tipoIdent2 = 12;    break;
					default:
					break;
				}
			}



    //System.out.println("token Izquierdo: "+tokenIzq.kind+"---"+tipoIdent1);
	//System.out.println("token Operador: "+ tokenOper.kind);
	//System.out.println("token Derecho: "+ tokenDer.kind+"----"+tipoIdent2);
	
	if(tipoIdent1 != tipoIdent2 ){
		//System.out.println("--------");
		return "Error sem\u00e1ntico: Los tipos de datos no son comparables [" + tokenIzq.image + ", " + tokenDer.image+"] --> Linea: "
							+ tokenIzq.beginLine+ ", Columna: " + tokenDer.beginColumn;	
	}//------------------String-------------boolean-------------char
	 else if(tipoIdent2 == 10 || tipoIdent2 == 11 ||tipoIdent2 == 12){
						//------------------- == ---------------------!=
			if ( tokenOper.kind != 29 && tokenOper.kind != 30 ){
				return "Error sem\u00e1ntico: Los operadores de comparacion no son validos en estos tipos de datos --> Linea: "
				+ tokenDer.beginLine + ", Columna: " + tokenDer.beginColumn;				
			}
	}
	return " ";
}

public static String checkFor( Token token ){

return " ";

}

public static String  setTokenOper(Token tokenOper){
	tokenOperador = tokenOper;
	return tokenOper.image;
}


static int x; 
}



